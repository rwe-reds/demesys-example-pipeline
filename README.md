# Simple Example Project for Jenkins Pipeline

Jenkins pipelines are in the `pipeline` directory

## Dependencies

This project has a dependency on [gtest](https://github.com/google/googletest) and standard build tools (gcc/g++)
