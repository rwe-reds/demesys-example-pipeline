pipeline {
    agent any
    options {
        skipStagesAfterUnstable()
    }
    stages {
        stage('Build') {
            steps {
                // Build some application
                sh 'mkdir -p build'
                sh 'cd build; gcc -o main ../src/main.c'
            }
        }
        stage('Execute App') {
            steps {
                // Run the application
                sh 'cd build; ./main'
            }
        }
        stage('Build Tests') {
            steps {
                // Build a test suite
                sh 'cd build; g++ -o unit_tests ../tests/unit_tests.cpp -lpthread -lgtest'
            }
        }
        stage('Test') {
            steps {
                // Run the test suite
                sh 'mkdir -p reports'
                sh 'cd build; ./unit_tests --gtest_output=xml:../reports/'
                // junit is another a Pipeline step (provided by the JUnit plugin) for aggregating test reports.
                junit 'reports/**/*.xml'
            }
        }
        stage('Deploy') {
            steps {
                // Execute the deploy steps
                sh 'echo "deploying ..."'
            }
        }
    }
}
