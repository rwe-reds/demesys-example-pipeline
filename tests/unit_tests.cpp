#include <gtest/gtest.h>

/*
 * Install libgtest in ubuntu :
 * sudo apt-get install libgtest-dev
 *
 * This will install the sources in /usr/src/gtest/
 *
 * sudo apt-get install cmake # install cmake
 * cd /usr/src/gtest
 * sudo cmake -DBUILD_SHARED_LIBS=ON
 * sudo make
 * sudo make install
 */

/*
 * Compile :
 * Dynamic :
 * g++ -o unit_tests unit_tests.cpp -lpthread -lgtest
 * Static :
 * g++ -o unit_tests unit_tests.cpp /usr/local/lib/libgtest.a -lpthread
 */
TEST(Pass, AlwaysPass) {
    ASSERT_EQ(1,1);
}

TEST(Fail, AlwaysFail) {
    ASSERT_EQ(0,1);
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    auto ret = RUN_ALL_TESTS();

    return 0;
}
